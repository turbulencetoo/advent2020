use std::collections::HashMap;
use regex::Regex;

const FIELDS: [&str; 8] = ["byr", "iyr", "eyr", "hgt",
                           "hcl", "ecl", "pid", "cid"];
#[derive(Debug)]
struct Passport {
    fields: HashMap<String, String>
}

impl Passport {
    fn has_correct_fields(&self) -> bool {
        FIELDS.iter()
            .filter(|&&s| s != "cid")
            .all(|&s| self.fields.contains_key(s))
    }

    fn fields_are_valid(&self) -> bool {
        // assumes we've verified the fields are in the map
        if !&self.fields["byr"].parse::<u32>()
            .map_or(false, |num| (num >= 1920) && (num <= 2002)) {
            return false;
        }
        if !self.fields["iyr"].parse::<u32>()
            .map_or(false, |num| (num >= 2010) && (num <= 2020)) {
            return false;
        }
        if !self.fields["eyr"].parse::<u32>()
            .map_or(false, |num| (num >= 2020) && (num <= 2030)) {
            return false;
        }
        let hgt_valid = {
            let caps = Regex::new("(\\d+)(cm|in)").unwrap().captures(&self.fields["hgt"]);
            match caps {
                None => false,
                Some(caps) => {
                    let num = caps.get(1).unwrap().as_str().parse::<u32>().unwrap();
                    match caps.get(2).unwrap().as_str() {
                        "cm" => (num >= 150) && (num <= 193),
                        "in" => (num >= 59) && (num <= 76),
                        _ => panic!("must be cm or in! I already validated this!")
                    }
                }
            }
        };
        if !hgt_valid {
            return false;
        }
        if !Regex::new("^#[0-9a-f]{6}$").unwrap().is_match(&self.fields["hcl"]) {
            return false;
        }
        if !["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].iter().
            any(|&clr| clr == self.fields["ecl"]) {
            return false;
        }
        if !Regex::new("^\\d{9}$").unwrap().is_match(&self.fields["pid"]) {
            return false;
        }
        true
    }
}

impl From<&str> for Passport {
    fn from(input: &str) -> Self {
        let mut map = HashMap::new();
        for &field in FIELDS.iter() {
            let rx = Regex::new(&format!("{}:(?P<{}>\\S+)", field, field)).unwrap();
            let caps = rx.captures(input);
            let data = match caps {
                Some(cap) => {
                    cap.name(field).unwrap().as_str()
                }
                None => continue
            };
            map.insert(String::from(field), String::from(data));
        }
        Passport { fields: map }
    }
}

fn get_input() -> Vec<Passport> {
    // first flatten out the blank-line separated thing
    let mut passport_strings = Vec::new();
    let mut cur = String::new();
    for line in include_str!("input.txt").lines() {
        let trimmed = line.trim();
        if trimmed.is_empty() {
            passport_strings.push(cur);
            cur = String::new();
        }
        else {
            cur.push_str(trimmed);
            cur.push(' ');
        }
    }
    if cur.is_empty() {
        passport_strings.push(cur);
    }

    passport_strings.iter()
        .map(|s| Passport::from(s as &str))
        .collect()
}

pub fn run_a() {
    let ans = get_input().iter()
        .filter(|p| p.has_correct_fields())
        .count();
    println!("Day 4a: number of valid passports is {}", ans);
}


pub fn run_b() {
    let ans = get_input().iter()
        .filter(|p| p.has_correct_fields())
        .filter(|p| p.fields_are_valid())
        .count();
    println!("Day 4b: number of valid passports is {}", ans);
}
