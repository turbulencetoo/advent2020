use itertools::Itertools;

fn get_input() -> Vec<u32>{
    include_str!("input.txt")
        .lines()
        .map(|line| line.trim().parse::<u32>())
        .collect::<Result<Vec<_>,_>>()
        .unwrap()
}


// Find the `n` numbers that add up to `sum` and return their product
fn get_product_from_sum(input: Vec<u32>, sum: u32, n: usize) -> u32 {
    input.into_iter()
        .combinations(n)
        .filter_map(|v: Vec<u32>| {
            if v.iter().sum::<u32>() == sum {
                Some(v.iter().product::<u32>())
            } else {
                None
            }
        })
        .next()
        .unwrap()
}


pub fn run_a() {
    let input = get_input();
    let ans = get_product_from_sum(input, 2020, 2);
    println!("Day 1a: The two numbers multiply to {}", ans)
}


pub fn run_b() {
    let input = get_input();
    let ans = get_product_from_sum(input, 2020, 3);
    println!("Day 1b: The three numbers multiply to {}", ans);
}
