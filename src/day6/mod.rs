use std::collections::HashSet;

// flatten the input so each customs group is a vec of strings
fn input_to_groups() -> Vec<Vec<String>> {
    let mut out = Vec::new();
    let mut cur = Vec::new();
    for mut line in include_str!("input.txt").lines() {
        line = line.trim();
        if line.is_empty() {
            out.push(cur);
            cur = Vec::new();
        }
        else {
            cur.push(String::from(line))
        }
    }
    if !cur.is_empty() {
        out.push(cur);
    }
    out
}

//
// Stuff for part A
//
fn group_to_set<T>(group: &[T]) -> HashSet<char>
where T: AsRef<str>
{
    group.iter()
        .flat_map(|s| s.as_ref().chars())
        .collect()
}

// get the set of answered questions for each group
fn groups_to_sets(groups: &[Vec<String>]) -> Vec<HashSet<char>> {
    groups.iter()
        .map(|group| group_to_set(group))
        .collect()
}

//
// stuff for part B
//
fn group_to_intersection<T>(group: &[T]) -> Vec<char>
where T: AsRef<str>
{
    // letters that appear in all strings in the group
    ('a'..='z')
        .filter(|&c| group.iter().all(|s| (s.as_ref() as &str).contains(c)))
        .collect()
}

fn groups_to_intersections(groups: &[Vec<String>]) -> Vec<Vec<char>> {
    groups.iter()
        .map(|group| group_to_intersection(group))
        .collect()
}

pub fn run_a() {
    let sets = groups_to_sets(&input_to_groups());
    let ans: usize = sets.iter()
        .map(|s| s.iter().count())
        .sum();
    println!("Day 6a: Then sum of all groups unique answers is {}", ans);
}

pub fn run_b() {
    let intersections = groups_to_intersections(&input_to_groups());
    let ans: usize = intersections.iter()
        .map(|g| g.len())
        .sum();

    println!("Day 6b: Then sum of all groups common answers is {}", ans);
}