use regex::Regex;

#[derive(Debug)]
struct PasswordRequirement {
    a: u32,
    b: u32,
    letter: char,
    password: String
}

impl PasswordRequirement {
    // Interpret 'a' as min and 'b' as max number of occurrences
    fn meets_requirement_part_a(&self) -> bool {
        let count = self.password
            .chars()
            .filter(|&c| c == self.letter)
            .count() as u32;
        (count >= self.a) && (count <= self.b)
    }
    // Interpret 'a' and 'b' a 1-indexed indices that show the two spots
    // one of which must be the char in question
    fn meets_requirement_part_b(&self) -> bool {
        let char_a = self.password.chars().nth((self.a - 1) as usize).unwrap();
        let char_b = self.password.chars().nth((self.b - 1) as usize).unwrap();
        (char_a == self.letter) ^ (char_b == self.letter)
    }
}

fn parse_line(input_line: &str) -> PasswordRequirement {
    let re = Regex::new(r"^(?P<a>\d+)-(?P<b>\d+) (?P<letter>.): (?P<password>.+)$").unwrap();
    let caps = re.captures(input_line).unwrap();

    PasswordRequirement {
        a: caps.name("a").unwrap().as_str().parse::<u32>().unwrap(),
        b: caps.name("b").unwrap().as_str().parse::<u32>().unwrap(),
        letter: caps.name("letter").unwrap().as_str().chars().next().unwrap(),
        password: String::from(caps.name("password").unwrap().as_str())
    }
}

fn get_input() -> Vec<PasswordRequirement>{
    include_str!("input.txt")
        .lines()
        .map(|l| parse_line(l.trim()))
        .collect()
}


pub fn run_a() {
    let input = get_input();
    let num_valid = input.iter()
        .filter(|pass| pass.meets_requirement_part_a())
        .count();
    println!("Day 2a: number of valid passwords is {}", num_valid);
}


pub fn run_b() {
    let input = get_input();
    let num_valid  = input.iter()
        .filter(|pass| pass.meets_requirement_part_b())
        .count();
    println!("Day 2b: number of valid passwords is {}", num_valid);
}
