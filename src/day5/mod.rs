#[derive(Debug)]
struct Seat {
    row: usize,
    col: usize
}

impl Seat {
    fn seat_id(&self) -> usize {
        self.row * 8 + self.col
    }
}

impl From<&str> for Seat {
    fn from(input: &str) -> Self {
        // treat lower bound as inclusive, upper bound as exclusive
        let mut row_max: usize = 128;
        let mut row_min: usize = 0;
        let mut col_max: usize = 8;
        let mut col_min: usize = 0;

        for chr in input.chars() {
            match chr {
                'F' => { row_max -= (row_max - row_min) / 2 },
                'B' => { row_min += (row_max - row_min) / 2 },
                'L' => { col_max -= (col_max - col_min) / 2 },
                'R' => { col_min += (col_max - col_min) / 2 },
                _ => panic!("Not acceptable char {}", chr)
            };
        }
        assert_eq!(row_min + 1, row_max);
        assert_eq!(col_min + 1, col_max);
        Seat { row: row_min, col: col_min }
    }

}


fn get_input() -> Vec<Seat> {
    include_str!("input.txt").lines()
        .map(|l| l.trim())
        .map(|l| Seat::from(l))
        .collect()
}


pub fn run_a() {
    let seats = get_input();
    let ans = seats.iter()
        .map(|seat| seat.seat_id())
        .max()
        .unwrap();
    println!("Day 5a: max seat ID is {}", ans);
}


pub fn run_b() {
    let mut sorted_seat_ids: Vec<usize> = get_input()
        .iter()
        .map(|s| s.seat_id())
        .collect();
    sorted_seat_ids.sort();

    let first_seat_id = sorted_seat_ids[0];

    // first seat to not equal 1 + previous seat
    let mut ans = sorted_seat_ids.iter()
        .enumerate()
        .filter(|(i, &s)| s != i + first_seat_id)
        .map(|(_, &s)| s)
        .next()
        .unwrap();

    // your seat is the missing one before that
    ans = ans - 1;

    println!("Day 5b: your seat is {}", ans);
}


#[test]
fn test_create_seat() {
    // explained test case FBFBBFFRLR
    let seat_0 = Seat::from("FBFBBFFRLR");
    assert_eq!(seat_0.row, 44);
    assert_eq!(seat_0.col, 5);
    assert_eq!(seat_0.seat_id(), 357);
    // BFFFBBFRRR: row 70, column 7, seat ID 567.
    let seat_1 = Seat::from("BFFFBBFRRR");
    assert_eq!(seat_1.row, 70);
    assert_eq!(seat_1.col, 7);
    assert_eq!(seat_1.seat_id(), 567);
    // FFFBBBFRRR: row 14, column 7, seat ID 119.
    let seat_2 = Seat::from("FFFBBBFRRR");
    assert_eq!(seat_2.row, 14);
    assert_eq!(seat_2.col, 7);
    assert_eq!(seat_2.seat_id(), 119);
    // BBFFBBFRLL: row 102, column 4, seat ID 820.
    let seat_3 = Seat::from("BBFFBBFRLL");
    assert_eq!(seat_3.row, 102);
    assert_eq!(seat_3.col, 4);
    assert_eq!(seat_3.seat_id(), 820);
}
