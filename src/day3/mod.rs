// Return a 2d array where true is tree, and false is no tree
fn get_input() -> Vec<Vec<bool>>{
    include_str!("input.txt")
        .lines()
        .map(|l| l.trim()
            .chars()
            .map(|c| c == '#')
            .collect::<Vec<bool>>()
        )
        .collect()
}


fn num_trees_hit(forest: &[Vec<bool>], slope_x: usize, slope_y: usize) -> u64 {
    let y_iter = (0usize..forest.len()).step_by(slope_y);
    let x_iter = (0usize..).step_by(slope_x);
    let modulo = forest[0].len();
    y_iter
        .zip(x_iter)
        .filter(|&(y,x)| forest[y][x%modulo])
        .count() as u64
}


pub fn run_a() {
    let input = get_input();
    let ans = num_trees_hit(&input, 3, 1);
    println!("Day 3a: number of trees hit would be {}", ans);
}


pub fn run_b() {
    let input = get_input();
    let mut ans = 1u64;
    for &(slope_x, slope_y) in [(1,1), (3,1), (5,1), (7,1), (1,2)].iter() {
        let num_trees = num_trees_hit(&input, slope_x, slope_y);
        ans *= num_trees;
    }
    println!("Day 3b: product of all trees hit is {}", ans);
}
