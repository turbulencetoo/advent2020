mod day1;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;
// mod day7;
// mod day8;
// mod day9;
// mod day10;
// mod day11;
// mod day12;
// mod day13;
// mod day14;
// mod day15;
// mod day16;
// mod day17;
// mod day18;
// mod day19;
// mod day20;
// mod day21;
// mod day22;
// mod day23;
// mod day24;
// mod day25;

fn main() {
    day1::run_a();
    day1::run_b();

    day2::run_a();
    day2::run_b();

    day3::run_a();
    day3::run_b();

    day4::run_a();
    day4::run_b();

    day5::run_a();
    day5::run_b();

    day6::run_a();
    day6::run_b();

//     day7::run_a();
//     day7::run_b();

//     day8::run_a();
//     day8::run_b();

//     day9::run_a();
//     day9::run_b();

//     day10::run_a();
//     day10::run_b();

//     day11::run_a();
//     day11::run_b();

//     day12::run_a();
//     day12::run_b();

//     day13::run_a();
//     day13::run_b();

//     day14::run_a();
//     day14::run_b();

//     day15::run_a();
//     day15::run_b();

//     day16::run_a();
//     day16::run_b();

//     day17::run_a();
//     day17::run_b();

//     day18::run_a();
//     day18::run_b();

//     day19::run_a();
//     day19::run_b();

//     day20::run_a();
//     day20::run_b();

//     day21::run_a();
//     day21::run_b();

//     day22::run_a();
//     day22::run_b();

//     day23::run_a();
//     day23::run_b();

//     day24::run_a();
//     day24::run_b();

//     day25::run_a();
//     day25::run_b();
}
